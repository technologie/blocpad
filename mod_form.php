<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add compact form
 *
 * @package mod_compact
 * @copyright  2006 Jamie Pratt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_compact_mod_form extends moodleform_mod {

    function definition() {
        global $PAGE;

        $PAGE->force_settings_menu();

        $mform = $this->_form;
		$config = get_config('compact');


        $mform->addElement('header', 'generalhdr', get_string('general'));

        // Add element for name.
        $mform->addElement('text', 'name', get_string('repere'), array('size' => '64'));
        $mform->addRule('name', null, 'required', null, 'client');
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addHelpButton('name', 'labelname', 'compact');

        $this->standard_intro_elements(get_string('labeltext', 'label'));
		
		//$this->standard_intro_elements(get_string('labeltext', 'label'));
		 
        // compact does not add "Show description" checkbox meaning that 'intro' is always shown on the course page.
        $mform->addElement('hidden', 'showdescription', 1);
        $mform->setType('showdescription', PARAM_INT);

		//Add element for activity image
		$mform->addElement('filemanager', 'image', get_string('image'), null, array('subdirs'=>0, 'maxfiles'=>1, 'accepted_types'=>'image'));
		$mform->addRule('image', null, 'required', null, 'client');
		
		// Add element for activityurl
        $mform->addElement('url', 'activityurl', get_string('activityurl', 'compact'), array('size'=>'42'), array('usefilepicker'=>true));
		$mform->addRule('activityurl', null, 'required', null, 'client');
        
		// Add element for synthesisurl
        $mform->addElement('url', 'synthesisurl', get_string('synthseysurl', 'compact'), array('size'=>'42'), array('usefilepicker'=>true));

		// Add element for videourl
        $mform->addElement('url', 'videourl', get_string('videourl', 'compact'), array('size'=>'42'), array('usefilepicker'=>true));

		// Add element for trainingsurl
        $mform->addElement('url', 'trainingurl', get_string('trainingurl', 'compact'), array('size'=>'42'), array('usefilepicker'=>true));

		// Add element for assessmenturl
        $mform->addElement('url', 'assessmenturl', get_string('assessmenturl', 'compact'), array('size'=>'42'), array('usefilepicker'=>true));


        $this->standard_coursemodule_elements();

//-------------------------------------------------------------------------------
// buttons
        $this->add_action_buttons(true, false, null);

    }

    function data_preprocessing(&$default_values) {
        if ($this->current->instance) {
            // editing existing instance - copy existing files into draft area
            $draftitemid = file_get_submitted_draft_itemid('files');
            file_prepare_draft_area($draftitemid, $this->context->id, 'mod_folder', 'content', 0, array('subdirs'=>true));
            $default_values['files'] = $draftitemid;
        }
    }

    /**
     * Override validation in order to make name field non-required.
     *
     * @param array $data
     * @param array $files
     * @return array
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        // Name field should not be required.
        if (array_key_exists('name', $errors)) {
            if ($errors['name'] === get_string('required')) {
                unset($errors['name']);
            }
        }
        return $errors;
    }

}
