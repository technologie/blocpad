# BlocPad



## Descriptif du projet

Créer une activité moodle (mod) qui permet d'améliorer l'ergonomie d'accès aux ressources didactiques nécessaires au suivi d'une activité en cours. L'activité met en forme des accès à d'autres ressources déjà présentes dans moodle (ou en externe). Elle nécessite ainsi de déposer une image d'illustration pour l'image et la définition de cinq liens pour accéder aux ressources didactiques.

Objectif de rendu final :
![IMAGE_DESCRIPTION](https://nuage03.apps.education.fr/index.php/s/CyGb2zWye2xbMmb/download/BlocPAD.png)


## Cahier des charges
...



## Add your files


```
cd existing_repo
git remote add origin https://forge.apps.education.fr/technologie/blocpad.git
git branch -M main
git push -uf origin main
```

## Installation dans Moodle

1. Télécharger le zip
2. Dézipper
3. Déposer le contenu du répertoire dans le dossier /mod de moodle
4. Accéder en admin à moodle et suivre le processus d'installation



## Roadmap
A définir

## Contributing
A la recherche de contributeurs


## License
For open source projects, say how it is licensed.

## Project status
Tout début...
