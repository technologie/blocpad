<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'compact', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package mod_compact
 * @copyright  2024 REMY Emmanuel
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configdndmedia'] = 'Offer to create a TECHNO Pad link when media files are dragged and dropped onto a course.';
$string['configdndresizeheight'] = 'When a Text and media area is created from a dragged and dropped media file, resize it if it is higher than this many pixels. If set to zero, the media file will not be resized.';
$string['configdndresizewidth'] = 'When a Text and media area is created from a dragged and dropped media file, resize it if it is wider than this many pixels. If set to zero, the media file will not be resized.';
$string['dndmedia'] = 'Media drag and drop';
$string['dndresizeheight'] = 'Resize drag and drop height';
$string['dndresizewidth'] = 'Resize drag and drop width';
$string['dnduploadcompact'] = 'Add media to course page';
$string['dnduploadcompacttext'] = 'Add a TECHNO Pad link to the course page';
$string['indicator:cognitivedepth'] = 'Text and media area cognitive';
$string['indicator:cognitivedepth_help'] = 'This indicator is based on the cognitive depth reached by the student in a Text and media area resource.';
$string['indicator:cognitivedepthdef'] = 'TECHNO Pad link cognitive';
$string['indicator:cognitivedepthdef_help'] = 'The participant has reached this percentage of the cognitive engagement offered by the TECHNO Pad link resources during this analysis interval (Levels = No view, View)';
$string['indicator:cognitivedepthdef_link'] = 'Learning_analytics_indicators#Cognitive_depth';
$string['indicator:socialbreadth'] = 'TECHNO Pad link social';
$string['indicator:socialbreadth_help'] = 'This indicator is based on the social breadth reached by the student in a Text and media area resource.';
$string['indicator:socialbreadthdef'] = 'TECHNO Pad link social';
$string['indicator:socialbreadthdef_help'] = 'The participant has reached this percentage of the social engagement offered by the Text and media area resources during this analysis interval (Levels = No participation, Participant alone)';
$string['indicator:socialbreadthdef_link'] = 'Learning_analytics_indicators#Social_breadth';
$string['compact:addinstance'] = 'Add a new TECHNO Pad link';
$string['compact:view'] = 'View TECHNO Pad link';
$string['compactname'] = 'Title in course index';
$string['compactname_help'] = 'The title is only used to identify the Text and media area in the course index and for activity completion. If you leave it empty,  a title will be automatically generated using the first characters of the text.';
$string['compacttext'] = 'Text';
$string['modulename'] = 'TECHNO Pad link';
$string['modulename_help'] = 'TECHNO Pad link enables you to display a TECHNO Pad with 6 links on page.

You can use a Text and media area to:

* Split up a long list of course activities with a subheading or an image
* Display an embedded video directly on the course page
* Add a short description to a course section';
$string['modulename_link'] = 'mod/compact/view';
$string['modulenameplural'] = 'COMP-ACT';
$string['privacy:metadata'] = 'The COMP-ACT plugin does not store any personal data.';
$string['pluginadministration'] = 'COMP-ACT administration';
$string['pluginname'] = 'COMP-ACT';
$string['search:activity'] = 'COMP-ACT';
